package jse25;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, IOException {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("FN1","LN1", LocalDate.now(),"emeail1"));
        personList.add(new Person("FN2","LN2", LocalDate.now(),""));
        personList.add(new Person("FN3","LN3", LocalDate.now(),null));

        ObjectListUtil objectListUtil = new ObjectListUtil();
        System.out.println("-----------------Список 1 jackson csv");
        objectListUtil.toCSV(System.out,personList);
        System.out.println("-----------------Список 1 csv");
        objectListUtil.customToCSV(System.out,personList);

        personList.clear();//если закомментировать то будет IllegalArgumentException
        personList.add(new WorkPerson("WPFN4","WPLN4",LocalDate.now(),"WPemail4","WPTN4"));
        personList.add(new WorkPerson("WPFN5","WPLN5",LocalDate.now(),"WPemail5","WPTN5"));

        System.out.println("-----------------Список 2 jackson csv");
        objectListUtil.toCSV(System.out,personList);
        System.out.println("-----------------Список 2 csv");
        objectListUtil.customToCSV(System.out,personList);

    }




}
