package jse25;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class ObjectListUtil<T extends Object> {
    List<Field> fieldList=new ArrayList();
    public void collectField(Object o,Class clazz){
        if(clazz==null) {
            return;
        }
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            fieldList.add(field);
        }
        collectField(o,clazz.getSuperclass());
    }
    public void toCSV(OutputStream outputStream, List<T> objectList) throws IOException {
        if (objectList.size()==0)return;
        String currentType = objectList.get(0).getClass().getName();
        for (Object o : objectList) {
            if(!currentType.equals(o.getClass().getName())) throw  new IllegalArgumentException("getObjectsDescription Объекты в списке должны быть одного типа "+currentType+" отличается от "+o.getClass().getName());
        }
        CsvMapper csvMapper=new CsvMapper();
        csvMapper.configure(CsvGenerator.Feature.ALWAYS_QUOTE_STRINGS,true);

        Class clazz = objectList.get(0).getClass();
        CsvSchema csvSchema = csvMapper.schemaFor(clazz).withHeader();
        ObjectWriter objectWriter =csvMapper.writerFor(clazz).with(csvSchema);
        objectWriter.writeValues(outputStream).writeAll(objectList);
    }
    public void headerToCSV(OutputStream outputStream) throws IOException {
        Boolean nadoRazdelitel = false;
        for(Field field:fieldList) {
            if(nadoRazdelitel)outputStream.write(",".getBytes());
            outputStream.write(('"'+field.getName()+'"').getBytes());
            nadoRazdelitel=true;
        }
        outputStream.write('\n');
    }
    public void fieldToCSV(OutputStream outputStream,Object o,Field field) throws IllegalAccessException, IOException {
        Boolean nadoRazdelitel = false;
        Object fieldValue=field.get(o);
        if(fieldValue==null) return;
        outputStream.write(('"'+fieldValue.toString()+'"').getBytes());
    }
    public void lineToCSV(OutputStream outputStream,Object o) throws IOException, IllegalAccessException {
        Boolean nadoRazdelitel = false;
        for(Field field:fieldList) {
            if(nadoRazdelitel)outputStream.write(",".getBytes());
            fieldToCSV(outputStream,o,field);
            nadoRazdelitel=true;
        }
        outputStream.write('\n');
    }
    public void customToCSV(OutputStream outputStream, List<T> objectList) throws IOException, IllegalAccessException {
        if (objectList.size()==0)return;
        String currentType = objectList.get(0).getClass().getName();
        fieldList.clear();
        collectField(objectList.get(0),objectList.get(0).getClass());
        headerToCSV(outputStream);
        for (Object o : objectList) {
            if(!currentType.equals(o.getClass().getName())) throw  new IllegalArgumentException("getObjectsDescription Объекты в списке должны быть одного типа "+currentType+" отличается от "+o.getClass().getName());
            lineToCSV(outputStream,o);
        }
    }
}
